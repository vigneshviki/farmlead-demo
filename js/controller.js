function Controller(offerNumber) {
    var service = new Service();
    var showOffer = service.getOffer(offerNumber,function(data){
        data = JSON.parse(data);
        var imagesList = "";
//        console.log(data);
        if((data !== null) || (data !== "")) {
            var today = moment().utc();
            var endDate= moment(data.offer.expiration);
            var duration = moment.duration(endDate.diff(today));
            var hours = Math.round(duration.asHours());
            var days = Math.round(duration.asDays());

            $("#offer-id").html("#" + data.offer.id);
            $("#offer-view-count-times").html(data.offer.visits);
            $("#grain-name").html(data.offer.grain_name + " - " + data.offer.grade_name);
            $("#season-name").html(data.offer.season_name);
            $("#transport-name").html(data.offer.transport_name);
            $("#region").html(data.offer.region.city_name + ", " + data.offer.region.province_name);
            $("#distance_from_to").html(data.offer.distance.distance.km + " km " + data.offer.distance.compass + " from " + data.offer.distance.from.city_name + ", " + data.offer.distance.from.province_name);
            if (data.offer.will_deliver == 0) {
                $("#willing-to-deliver").html("NO");
            } else {
                $("#willing-to-deliver").html("YES");
            }
            $("#stored-in").html(data.offer.grain_storage_name);
            $("#movement-year").html(data.offer.delivery_year);
            $("#movement-period").html(data.offer.delivery_name);
            $("#user-rating").html(data.offer.user_rating + " / 10");
            if (data.offer.is_certified_organic_farmer == 0) {
                $("#certified-organic").html("No").addClass("grey");
            } else {
                $("#certified-organic").html("Yes").addClass("black");
            }

            for (var i = 0; i < data.offer.images.length; i++) {
                imagesList += '<img src="' + data.offer.images[i].full + '">';
            }
            $("#offer-images-wrapper").html(imagesList);
            if (data.offer.moisture === null) {
                $("#grain-moisture").html("-");
            } else {
                $("#grain-moisture").html(data.offer.moisture + "%").addClass("black");
            }
            if (data.offer.organic_name == "Non Organic") {
                $("#grain-organic").html("No").addClass("grey");
                $("#certified-organic").parent().hide();
            } else {
                $("#certified-organic").parent().show();
                $("#grain-organic").html("Yes").addClass("black");
            }
            if (data.offer.comment !== null) {
                $("#offer-comments").html(data.offer.comment);
            }

            if((hours <= 0) && (days <= 0)){ //expired
                $("#offer-expiry").html("Not on Marketplace").addClass("red");
            }else if((hours > 0) && (days == 0)){ //less than 24 hours
                if(hours == 1){
                    $("#offer-expiry").html(hours+" hour left").addClass("black"); // one hour left
                }else{
                    $("#offer-expiry").html(hours+" hours left").addClass("black"); // more than one hour left but less than 1 day
                }
            }else if(days > 0){ //more than 24 hours
                if(days == 1){
                    $("#offer-expiry").html(days+" day left").addClass("grey"); // one day left
                }else{
                    $("#offer-expiry").html(days+" days left").addClass("grey"); // more than one day left
                }
            }
        }
    });
}

var controller = Controller("34462");

