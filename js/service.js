function Service() {
    var url="https://api.farmlead.com/api/v2/offer/";

    this.getOffer = function(offerNumber,callback){
        var offer = parseInt(offerNumber);
        $.ajax({
            url: url+offer,
            method:"POST",
            contentType: "application/json"
        }).done(function(data) {
            return callback(data);
        }).fail(function() {
            var data = '{"result": true, "offer": {"comment": null, "bonded_by": "", "is_comment_verified": null, "delivery_year": 2016, "grain_id": 24, "delivery_name": "Immediately", "season_id": 90, "close_date": null, "images": [{"full": "https://s3.amazonaws.com/fl2-prod-resized-image/source/Pic_Available_Soon.png", "thumb": "https://s3.amazonaws.com/fl2-prod-resized-image/source/Pic_Available_Soon.png"}], "grade_id": 1, "transport_id": 94, "grain_name": "Corn", "transport_name": "FOB", "user_id": 4165, "is_bonded": 0, "delivery_distance": {"mi": 0, "km": 0}, "upload_uuid": "26F597F7-03A3-4BC4-9572-9D70642F12B6", "id": 34462, "user_rating": 9.03, "moisture": null, "grain_storage_id": 71, "repost": 1, "type": 0, "grain_storage_name": "Flat Bottom Bin", "season_name": "2016/2017", "organic_farmer_certifier": null, "rail_option_name": null, "organic_id": 97, "rail_option_id": null, "visits": 1922, "organic_name": "Non Organic", "removed": "2016-09-30 19:01:03", "accurate_quantity": {"8": 1000.0, "10": 56000, "11": 560.0, "12": 25.401173}, "distance": {"distance": {"mi": 2743.974336, "km": 4416}, "from": {"city_id": 5460, "city_name": "Winnipeg", "province_id": 5, "province_name": "MB"}, "compass": "NW"}, "will_deliver": 0, "created": "2016-09-30 19:00:07", "region": {"city_id": 9805, "city_name": "Anchorage", "province_id": 62, "province_name": "AK"}, "delivery_id": 13, "modified": "2017-05-16 22:58:00", "is_certified_organic_farmer": 0, "grade_name": "#1", "expiration": "2016-10-14 19:00:08", "car_order_id": null}}';
            return callback(data);
        })
    }
}
